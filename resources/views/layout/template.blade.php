<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JNR Consult - @yield('title') </title>

    <!-- page icon -->
    <link rel="shortcut icon" href="/images/site_logo.png">

    <!-- Bootstrap core CSS (CDN for deployment )  -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.css">

    <style type="text/css">


      html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                margin: 0;
            }

      .navbar-toggle {
          background-color: rgba(45,108,49,1) !important;
          border: none !important;
      }

      nav, main, footer{ float: left; }
      main, footer, nav{ width: 100% }
      main{ margin:70px 0 }

      .header-nav .nav.navbar-nav li a:hover{
        background-color: rgb(45,108,49);
        color: #fff
      }

      footer .nav.navbar-nav li a:hover{
        background-color: rgba(45,108,49, 0.6);
        color: #fff
      }

      .navbar-brand>img {
          display: inline-block;
          margin-top: -10px;
      }

      footer .navbar-nav>li {
          background-color: rgba(0,0,0,0);
          border-bottom: 1px solid #fff;
          padding: 0;
      }

      .navbar-nav>li {
          float: left;
          padding: 10px 10px 0 10px;
          border-bottom: 1px solid black;
          margin-left: 10px;
      }
      a.navbar-brand, .navbar-nav>li>a {
          color: #000;
      }
      .navbar-toggle .icon-bar {
          background-color: #fff;
      }
      .navbar-toggle {
          background-color: #000;
          border: #000;
      }
      body{
        float: left;
        width: 100%;
      }

      .dropdown-menu li a {
        padding: 10px;
      }

      footer ul.nav.navbar-nav{ background-color: rgba(0,0,0,0) }

      footer ul.nav.navbar-nav{ width: 100% }

      footer nav ul li {
          width: 46%;
          text-align: center;
      }

      footer nav ul {
          background-color: rgba(0,0,0,0);
      }

      nav.header-nav{
        border-bottom: 1px solid rgba(45,108,49, 0.5);
        background-color: rgba(255,255,255, 0.9);
      }

      footer{
        color: #fff;
        background-color: rgba(45,108,49, 0.8);
        padding: 20px 0;
      }

      footer .navbar-nav>li>a {
          color: #fff;
      }

      footer.container-fluid{
        padding: 0
      }

      footer center{
        padding-bottom: 10px;
        clear:left
      }

      div.header-container{
        margin-bottom: 20px; 
        padding: 0 10px 10px 10px;
        color: #fff;
      }

      div.header-container div.header-line-thing{
        width: 4px;
        height: 40px;
        background-color: #fff;
        display: inline-block;
      }

      div.header-container .text-center{
        display: inline-block; 
        margin: 0; 
        padding: 10px;
      }

      @media (max-width: 768px){

        .header-nav .navbar-nav>li {
          float: none;
          background-color: rgba(45,108,49, 0.8);
          border-bottom: 1px solid #fff;
        }

        .header-nav .navbar-nav>li>a {
          color: #fff;
        }

      }

    </style>
  </head>

  <body style="background-color: rgb(0,214,0)">

    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-xs-12" style="background-color:#fff; padding:0">

      <!-- Navigation -->
      <nav class="header-nav navbar navbar-fixed-top col-lg-10 col-lg-offset-1" data-spy="affix" data-offset-top="50">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="/"><img alt="company logo" src="/images/site_logo.png" /></a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/" id="home-link">Home</a></li>
              <li><a href="/services" id="services-link">Services</a></li>
              <li><a href="/projects" id="projects-link">Projects</a></li>
              <li><a href="contact-us" id="contact-us-link">Contact Us</a></li> 
            </ul>
          </div>
        </div>
      </nav>

      <!-- main content start -->
          <main>
              @yield('content')
          </main>

      <!-- Footer -->
      <footer>

        <nav class="navbar footer">
          <div class="container-fluid col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
            <ul class="nav navbar-nav">
              <li><a href="http://jnr-consult.org/webmail?For%20official%20use%20only...">Mail</a></li>
              <li><a href="/contact-us">Contact Us</a></li>
            </ul>
          </div>
        </nav>

        <center>
          <div>
            <i class="fa fa-facebook-square" style="font-size:36px"></i>
            <i class="fa fa-twitter-square" style="font-size:36px"></i>
            <i class="fa fa-linkedin-square" style="font-size:36px"></i>
          </div>
        </center>

        <div class="container col-xs-12">
          <p class="m-0 text-center text-white">Copyright &copy; JNR Consult 2017</p>
        </div>
      </footer>

    </div>

    <!-- Bootstrap core JavaScript (CDN) usually reliable -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    @yield('scripts')

  </body>

</html>
