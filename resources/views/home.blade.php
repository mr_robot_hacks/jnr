@extends('layout.template')
  
@section('title')
  Home
@endsection

@section('content')
  
  <style type="text/css">
    #home-link {
          background-color: rgb(45,108,49);
          color: #fff
      }

/*#####################
Additional Styles (required)
#####################*/

#myCarousel .list-group .active {
    background-color: rgb(45,108,49) !important;
}

#myCarousel .carousel-caption {
    left:0;
	right:0;
	bottom:0;
	text-align:left;
	padding:10px;
	background:rgba(0,0,0,0.6);
	text-shadow:none;
}
 
#myCarousel .list-group {
	position:absolute;
	top:0;
	right:0;
}
#myCarousel .list-group-item {
	border-radius:0px;
	cursor:pointer;
}
#myCarousel .list-group .active {
	background-color:#eee;	
}

.list-group-item {
    background-color: rgba(0,0,0,0);
    border: none;
    color:rgb(255,190,0);
}

.intro-ps, .team-ps, .value-ps{
  padding:0 50px 0 0;
  text-align:justify;
}

.homepage-text{
  display:none;
}

.show-me{
  display:block !important;
}

.carousel-caption{
  display: none !important;
}

@media (min-width: 992px) { 
	#myCarousel {padding-right:33.3333%;}
	#myCarousel .carousel-controls {display:none;} 	
}
@media (max-width: 991px) { 
	.carousel-caption p,
	#myCarousel .list-group {display:none;} 
}

  </style>
<div class="container col-xs-12">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">


        <div class="item active" data-content="introduction-text">
          <img src="/images/jnr-consult.png">
          <!-- <div class="carousel-caption">
            <h4><a href="#">tempor invidunt ut labore et dolore magna aliquyam erat</a></h4>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
          </div> -->
        </div>
        
        <div class="item" data-content="quality-text">
          <img src="/images/nkusi-river.jpg">
           <div class="carousel-caption">
            <h4>Nkusi: Nov 2014 - May 2015:</h4>
            <p>Environmental Impact Assessment and preparation of ESMP for the proposed construction of Nkusi Hydropower (9.6.MW) – in Kibaale & Hoima Districts. JNR Consult was sub-contracted by Aster-Integral Private Limited.</p>
          </div>
        </div>
 
         <div class="item" data-content="team-text">
          <img src="/images/community-meeting.jpg">
           <div class="carousel-caption">
            <h4>Lira - Gulu - Nebbi - Arua September 2015 – December 2015:</h4>
            <p>132 kV Electricity Transmission Project. Resettlement Action Plan for the proposed 132 kV Lira- Gulu-Nebbi-Arua Transmission Line project,315KM (starting from Lira substation). JNR Consult Ltd is sub-consultant to WSP (Canada) Client: Uganda Electricity Transmission Company Limited. Funder: World Bank.</p>
          </div>
        </div>
        
        <div class="item" data-content="values-text">
          <img src="/images/staff-audit.jpg">
           <div class="carousel-caption">
            <h4>Environmental and Social Compliance Monitoring for Muvumbe Hydro power project. April 2016-June 2017:</h4>
            <p>JNR-Consult was contracted by Muvumbe Hydro Uganda Ltd. to carry out quarterly monitoring of Environmental and Social Compliance including occupational Health and Saftey during construction. It also involves preparation of reports to be submitted by the client to funders and regulatory authorities in Uganda. Client: Muvumbe Hydro Uganda Ltd. Funder: KfW/GETFIT.</p>
          </div>
        </div>
        
        <div class="item" data-content="safety-text">
          <img src="/images/consultation-team.jpg">
           <div class="carousel-caption">
            <h4>Environment and Social Audit for Muvumbe Hydropower October - December 2016:</h4>
            <p>JNR-Consult was contracted by Muvumbe Hydro Uganda Ltd. to carry out Annual Environment and Social Audit for the project which is still under construction. Issues to audit include compliance to ESMP & ESAP, approval conditions for NEMA and other permits as well as IFC Performance Standards. Client: Muvumbe Hydro Uganda Ltd. Funder: KfW/GETFIT.</p>
          </div>
        </div>
                
      </div><!-- End Carousel Inner -->
 
 
    <ul class="list-group col-sm-4">
    <li data-target="#myCarousel" data-slide-to="0" class="list-group-item active"><h4>JNR</h4></li>
      <li data-target="#myCarousel" data-slide-to="1" class="list-group-item"><h4>Quality Policy.</h4></li>
      <li data-target="#myCarousel" data-slide-to="2" class="list-group-item"><h4>The Team.</h4></li>
      <li data-target="#myCarousel" data-slide-to="3" class="list-group-item"><h4>Values.</h4></li>
      <li data-target="#myCarousel" data-slide-to="4" class="list-group-item"><h4>Health, Safety and Environment Policy.</h4></li>
    </ul>
 
      <!-- Controls -->
      <div class="carousel-controls">
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
      </div>
 
    </div><!-- End Carousel -->
</div>

    <!-- Page Content -->
    <div class="container col-xs-12">
      <!-- Portfolio Item Row -->
      <div class="row">

        <h4 style="color:rgb(0,214,0)" class="col-md-12"><strong><?php echo date("Y/m/d") ?></strong></h4>

        <!-- intro text begin -->
          <div class="col-md-12 homepage-text show-me" id="introduction-text">
            <h3 class="my-3" style="color:rgb(0,214,0)"><strong>Introduction</strong></h3>

            <p class="intro-ps">JNR provides quality consulting services in the key consulting areas of environmental and social impact assessments, resettlement action planning, environmental & social audit and compliance monitoring and, training with the aim of exceeding client expectations.</p>
            <p class="intro-ps">JNR CONSULT Limited was incorporated in Uganda in 2013 as a company to provide quality professional services to support sustainable development. It is registered with the Uganda Registrar of Companies (Reg. No: 173554). It is a dynamic, innovative company that provides Consultancy Services in Environment and development.</p>
            <p class="intro-ps">The Company works with both national and international private firms and government entities to ensure that development projects remain sustainable to local communities by effectively and efficiently managing, planning for and conserving the environment.</p>
          </div>
        <!-- intro text end -->

        <!-- quality text begin -->
          <div class="col-xs-12 homepage-text" id="quality-text">

            <h3 class="col-xs-11" style="color:rgb(0,214,0)">Quality Policy</h3>

            <p class="col-xs-11">
              JNR provides quality consulting services in the key consulting areas of environmental impact assessments, resettlement action planning, environmental & social audit and compliance and training with the aim of exceeding client expectations.
            </p>

            <h4 class="col-xs-11">Vision</h4>
            <p class="col-xs-11">
              The leading environment and social services consultancy in East African Region.
            </p>

            <h4 class="col-xs-11">Mission</h4>
            <p class="col-xs-11">
              Provide impeccable environment and social impact assessment, resettlement action plan  and implementation, compliance monitoring, Audit and traning services to our customers.
            </p>
            
            <h4 class="col-xs-11">Our commitment</h4>
            <p class="col-xs-11">
              JNR is committed to:
            </p>
            <ul class="col-xs-11" style="margin-left: 40px;">
              <li class="col-xs-11">
                Identify, monitor and review quality objectives.
              </li>
              <li class="col-xs-11">
                Consistently delivery superior environmental impact assessments, resettlement action planning, environmental & social audit and compliance and training services to our clients and exceeding their expectations.
              </li>
              <li class="col-xs-11">
                Continually improving the Quality Management System through conducting periodic objective, risk and opportunity assessments and performance monitoring activities.
              </li>
              <li class="col-xs-11">
                Ensuring that our staff recieve instruction, information, supervision & training in the business.
              </li>
              <li class="col-xs-11">
                Abiding by the legal requirements of the industry in which we operate.
              </li>
              <li class="col-xs-11">
                Ensuring the health & safety of its employees and consultants on projects.
              </li>
              <li class="col-xs-11">
                Keeping abreast of technological changes in the industry.
              </li>
            </ul>

            <p class="col-xs-11">
              JNR will ensure that this policy is communicated to all staff and interested parties, understood, implemented and maintained. It will be reviewed annually.
            </p>

          </div>
        <!-- quality text end -->

        <!-- team text begin -->
          <div class="col-xs-12 homepage-text" id="team-text">

            <h3 class="col-xs-11" style="color:rgb(0,214,0)">The Team</h3>

            <p class="col-xs-11 team-ps">
            We have a team of trusted and accredited professionals who ensure that the company delivers the services in a professional and timely manner. The Company has close working relationships with credible national and international companies that are leading service providers in environment and development. 
            </p>

          </div>
        <!-- team text end -->

        <!-- values text begin -->
          <div class="col-xs-12 homepage-text" id="values-text">

            <h3 class="col-xs-11" style="color:rgb(0,214,0)">Core Values</h3>
            
            <p class="col-xs-11 value-ps">
            JNR believes in providing high-quality service to our clients and maintaining high levels of professional ethics and admirable core values to the satisfaction of the client needs and expectations. 
            </p>

            <p class="col-xs-11 value-ps">
            The company endeavors to develop excellent relationships with its clients, based on trust, honesty and integrity.
            </p>

          </div>
        <!-- values text end -->

        <!-- health and safety text -->

          <div class="col-xs-12 homepage-text" id="safety-text">

            <h3 class="col-xs-11" style="color:rgb(0,214,0)">Health, Safety and Environment Policy</h3>

            <p class="col-xs-11">
            Our vision is to be the leading environment, social and sustainable development consultancy in the East African region. However, this vision cannot be achieved without and internal commitment to the protecting the environment and the safety of the people with whom we work and engage in the broader community. We have a legal and moral obligation and responsibility to safeguard our employees and the public against harm caused by our operations.
            </p>

            <h4 class="col-xs-11">Our commitment</h4>
            <p class="col-xs-11">
              JNR is committed to:
            </p>
            <ul class="col-xs-11" style="margin-left: 40px;">
              <li class="col-xs-11">
                Comply with all applicable health, safety and environment laws, regulations and standards to which we subscribe.
              </li>
              <li class="col-xs-11">
                Develop and maintain a health, safety and environment culture among our employees and contractors.
              </li>
              <li class="col-xs-11">
                Minimise the risks of any health and safety hazards to the public and our employees by conducting risk assessments and implementing controls.
              </li>
              <li class="col-xs-11">
                Continually improve  the management of health and safety through setting and monitoring performance objectives against health and safety.
              </li>
              <li class="col-xs-11">
                Provide quality environment consulting services that will ultimately encourage the prevention of harm to the environment.
              </li>
              <li class="col-xs-11">
                Train our staff and the public on their responsibility towards ensuring healthy and safe practices and the protection of the environment.
              </li>
            </ul>

            <p class="col-xs-11">
              JNR will ensure that this policy is communicated to all staff and interested parties, understood, implemented and maintained. It will be reviewed annually.
            </p>

            </div>

        <!-- health and safety end -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    @section('scripts')
      
      <script>

         /*#####################
        Additional jQuery (required)
        #####################*/

        $(document).ready(function(){

          var boxheight = $('.carousel-inner').height();
          var itemlength = 4;
          var triggerheight = Math.round(boxheight/itemlength+1);
          
          var clickEvent = false;
          $('#myCarousel').carousel({
            interval:   9000	
          }).on('click', 'li', function() {
              clickEvent = true;
              $('.list-group li').removeClass('active');
              $(this).addClass('active');		
          }).on('slid.bs.carousel', function(e) {
            if(!clickEvent) {
              var count = $('.list-group').children().length -1;
              var current = $('.list-group li.active');
              current.removeClass('active').next().addClass('active');
              var id = parseInt(current.data('slide-to'));
              if(count == id) {
                $('.list-group li').first().addClass('active');	
              }
            }
            clickEvent = false;
          }).on('slid.bs.carousel', function () {
              shownext = $("#myCarousel .carousel-inner .item.active").attr("data-content");
              $(".homepage-text").removeClass("show-me");
              $("#"+ shownext).addClass("show-me");
          });;

        });
          
      </script>

    @endsection

    
@endsection