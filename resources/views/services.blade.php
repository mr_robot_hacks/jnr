@extends('layout.template')

@section('title')
  Our Services    
@endsection

@section('content')
  
  <style type="text/css">
    #services-link {
          background-color: #253f8e;
          color: #fff
      }
    
    .service-cubes { min-height: 300px }
    
    .service-cubes h3 { min-height: 65px; font-weight:600 }

    .services-header{
      border-bottom: 2px solid rgba(45,108,49,1); 
      padding-bottom: 10px;
      color: rgb(0,214,0);
      font-weight: 500;
    }

    .service-cubes ul li {
      color:#000
    }

    h4.services-header span {
      color: #e4c420;
      padding: 5px;
      font-size: 25px;
      font-weight: 400;
    }

  </style>

    <!-- Page Content -->
    <div class="container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12" style="margin-top: 50px;">

      <div class="header-container col-lg-6 col-md-5 col-sm-5 col-xs-9" style="background-color: rgb(255, 190, 0)">

        <div class="header-line-thing"></div>
        <h2 class="text-center">WHAT WE OFFER</h2>

      </div>

      <p class="col-xs-12">
        JNR CONSULT Limited provides professional, experienced and independent consultancy services in environment development and related disciplines to a wide range of clients in the public and private sector. Our truck record also include Non-Governmental Organizations, Community Based Organizations and individuals. The company is formed to provide a wide range of environment and development consulting services in the following fields.
      <p>

      <img alt="growth illustration" src="/images/services.png" class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style="clear:left; margin-bottom:20px" /> 

      <div class="col-lg-6 col-md-6 col-xs-12 service-cubes">
      <h4 class="col-xs-12 services-header"> Environmental and Social Management Services</h4>

      <ul>
        <li>Strategic Environmental Assessment (SEA)</li>
        <li>Ecosystem Services</li>
        <li>Environmental Screening Assessments/Risk Assessments</li>
        <li>Environmental and Social Impact Assessment (ESIA)</li>
        <li>Resettlement Action Plan (RAP)</li>
        <li>Resettlement Action Plan Implentation (RAPI)</li>
        <li>Environmental Site Compliance and Auditing</li>
        <li>Environmental Mornitoring and Site Compliance</li>
        <li>Environmental Site Compliance and Auditing</li>
        <li>We accord stakeholder engagement an important aspect in the service we provide and believe that the best, most cost effective and sustainable solutions come from an approach that fully engages all project stakeholders</li>
      </ul>

      </div>

      <div class="col-lg-6 col-md-6 col-xs-12 service-cubes">
      <h4 class="col-xs-12 services-header"> Professional Environmental Training</h4>

      <ul>
        <li>Tailored Environmental Awareness and EIA Training to corporate companies, clients and government agencies.</li>
        <li>Training young professionals in Environmental Management tools (SEA, Ecosystems services).</li>
        <li>Impact Assessment and Environmental auditing).</li>
      </ul>
      </div>

      <div class="col-lg-6 col-md-6 col-xs-12 service-cubes">
      <h4 class="col-xs-12 services-header"> Community Development Projects</h4>

      <ul>
        <li>Institutional building and management: Training, Institutional strategic planning and policy analysis.</li>
        <li>Designing and production of institutional guideline manuals</li>
        <li>Social economic Assessment Social economic assessment for development projects</li>
        <li>Preparation of proposals for developing livelihood mitigation and other community development programmes</li>
        <li>Monitoring and Evaluation: Designing of monitoring and evaluation systems</li>
      </ul>
      </div>

    </div>
    <!-- /.container -->

@endsection