@extends('layout.template')

@section('title')
    Contact Us
@endsection

@section('content')
	
	<style type="text/css">
		#contact-us-link {
	        background-color: #c88a33;
	        color: #fff
	    }
	</style>

	<!-- Container (Contact Section) -->
	<div id="contact" class="container-fluid bg-grey col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12" style="margin-top: 50px;">

	<div class="header-container col-lg-6 col-md-5 col-sm-5 col-xs-9" style="background-color: #c88a33">

		<div class="header-line-thing"></div>
		<h2 class="text-center">CONTACT US</h2>

	</div>

	@if(isset($message))
		<div class="alert alert-success" style="clear:both">
			<strong>Success!</strong> {{ $message }}
		</div>
	@endif

	<div class="col-xs-12" style="clear: left; margin-top:30px">
		
		<div class="col-lg-6" style="    border: 5px solid #fff; height: 200px; color: #fff; padding-top: 30px; background-color: rgba(45,108,49, 0.8);">

	      <p class="col-xs-12"><strong>Mr. Robert Ndyabarema</strong></p>
	      <p class="col-xs-12"><span class="glyphicon glyphicon-map-marker"></span> Managing Director</p>
	      <p class="col-xs-12"><span class="glyphicon glyphicon-phone"></span> +256 758417452</p>
	      <p class="col-xs-12"><span class="glyphicon glyphicon-envelope"></span> rndyaba@jnr-consult.org</p>	   

		</div>

	</div>

	  <div class="col-xs-12" style="clear: left; margin-top:50px">
	    <div class="col-sm-5">
	      <p>Send us and email and we'll get back to you as soon as we can.</p>
	      <p style="color:#253f8e"><span class="glyphicon glyphicon-map-marker"></span> Ham Towers Plot 923 Makerere Hill Road, Kampala Uganda</p>
	      <p style="color:#253f8e"><span class="glyphicon glyphicon-phone"></span> +256 483660013</p>
	      <p style="color:#253f8e"><span class="glyphicon glyphicon-envelope"></span> admin@jnr-consult.org</p>	   
	    </div>
	    <div class="col-sm-7 slideanim">
	    	<form action="/contact-us" method="post">

					{{ csrf_field() }}
		      <div class="row">
		        <div class="col-sm-6 form-group">
		          <input class="form-control" id="name" name="name" placeholder="Your Name" type="text" required>
		        </div>
		        <div class="col-sm-6 form-group">
		          <input class="form-control" id="email" name="email" placeholder="Your Email" type="email" required>
		        </div>
		      </div>
		      <textarea class="form-control" id="comments" name="comments" placeholder="Inquiries/ Questions" rows="5" required></textarea><br>
		      <div class="row">
		        <div class="col-sm-12 form-group">
		          <button class="btn btn-default pull-right" type="submit">Send</button>
		        </div>
		      </div>
	      <form>	
	    </div>
	  </div>

	</div>

	<div id="googleMap" style="height:300px;width:100%;"></div>

	<!-- Add Google Maps -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC99J6rFowiMloJVi2pQ4c7ZIYhM22uLEc"></script>
	<script>
		var myCenter = new google.maps.LatLng(0.328102, 32.570415);

		function initialize() {
		var mapProp = {
		  center:myCenter,
		  zoom:13,
		  scrollwheel:false,
		  draggable:false,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
		  };

		var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

		var marker = new google.maps.Marker({
		  position:myCenter,
		  });

		marker.setMap(map);
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>

@endsection  