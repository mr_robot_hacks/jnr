@extends('layout.template')
  
@section('title')
  Not Found
@endsection

@section('content')
  
    <div class="container col-xs-12">

        <img src="/images/jnr-consult.png" class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2" style="margin-top: 50px;" />
        <p class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12" style="margin-top: 20px;">
            Thank you for visiting the JNR Consult website. Unfortunnately we can't find what you're looking for. In the meantime, use the menu above to find your way around.
        </p>
        
    </div>
    
@endsection