@extends('layout.template')

@section('title')
  Projects
@endsection

@section('content')

<style type="text/css">
    #projects-link {
          background-color: rgb(255, 190, 0);
          color: #fff
      }

    .clients{ 
      color: rgba(45,108,49,1);
      font-weight: 100; }

    .project-sub-heading {
      background-color: rgb(255, 190, 0);
      color: #fff;
      padding: 5px;
      font-weight: 500;
      padding-left: 0;
    }

    .project-sub-heading span {
      background-color: #e4c420;
      padding: 5px;
      border-right: 10px solid #fff;
    }

    
    h4.project-name {
      color: rgb(0,214,0);
      font-weight: 500;
    }

    h4.project-name span {
      color: #e4c420;
      padding: 5px;
      font-size: 25px;
      font-weight: 400;
    }

    img.thumbnail{
      width: 70px;
      display:inline-block;
      margin-top:5px
    }

    hr {
      margin: 0
    }

    p.project-text{
      text-align: justify;
    }

  </style>

<div id="about" class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12" style="margin-top: 50px;">

  <div class="header-container col-lg-6 col-md-5 col-sm-5 col-xs-9" style="background-color: rgb(255, 190, 0)">

    <div class="header-line-thing"></div>
    <h2 class="text-center">PROJECTS</h2>

  </div>

  <div class="container col-xs-12">
    <div class="row">
      <div class="col-lg-12" style="clear: left;">
        <h4 class="section-subheading text-muted" style="font-weight:500">JNR CONSULT networks to source experienced professionals who provide multi-disciplinary services. Our team has hands on experience of managing projects on the ground from inception to completion.</h4>
      </div>
      <!-- /.row -->

      <h2 class="col-xs-12">Environmental Impact Assessment</h2>
      <h3 class="col-xs-12 project-sub-heading"><span>A.</span> Hydro Power Projects</h3>
      <!-- Project Two -->
      <div class="row">
        <div class="col-md-6">
            <img class="project-images img-fluid rounded mb-3 mb-md-0" src="/images/projects/muvumbe-project.jpg" style="width: 100%;" alt="" />
        </div>
        <div class="col-md-5">
          <h4 class='project-name'>Muvumbe Hydropower Jan 2015 - June 2015</h4>
          <p class="project-text">Updating of Environment and Social Management Plan (ESMP) and Preparation of Environment and Social Action Plan (ESAP) for Muvumbe Hydropower Project (7 MW) in Maziba Sub-county Kabale District. </p>
          
          <p class="clients"> Client: Muvumbe Hydro U Ltd.</p>
          
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <!-- Project Three -->
      <div class="row">
        <div class="col-md-6">
            <img class="project-images img-fluid rounded mb-3 mb-md-0" src="/images/projects/nkusi-project.jpg" alt="" style="width: 100%;" />
        </div>
        <div class="col-md-5">
          <h4 class='project-name'>Nkusi Hydropower Nov 2014 - May 2015</h4>
          <p class="project-text">Environmental Impact Assessment and preparation of ESMP for the proposed construction of Nkusi Hydropower (9.6.MW) – in Kibaale & Hoima Districts. JNR Consult was sub-contracted by Aster-Integral Private Limited. </p>
          
          <p class="clients"> Client: PA Technical Services. Funders: KfW /GETFIT</p>
          
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <h3 class="col-xs-12 project-sub-heading"><span>B.</span> Electricity Transmission Lines</h3>
      <div class="row">

        <div class="col-md-6">
            <img class="project-images img-fluid rounded mb-3 mb-md-0" src="/images/projects/lgna-project.jpg" alt="" style="width: 100%;" />
        </div>
        <div class="col-md-5">
          <h4 class='project-name'>Proposed 132 kV Lira- Gulu- Nebbi-Arua Transmission</h4>
          <p class="project-text">Proposed 132 kV Lira- Gulu- Nebbi-Arua Transmission June 2014 –Dec 2016: Environment and Social Impact Assessment for the proposed 132 kV Lira- Gulu- Nebbi-Arua Transmission Line project (starting from Lira substation). </p>
          
          <p class="clients"> Client: Uganda Electricity Transmission Company Limited Funders: World Bank.</p>
          
        </div>
      </div>

      <hr>

      <div class="row">

        <div class="col-md-6">
            <img class="project-images img-fluid rounded mb-3 mb-md-0" src="/images/projects/nkenda-bunia-project.jpg" alt="" style="width: 100%;" />
        </div>
        <div class="col-md-5">
          <h4 class='project-name'>Nkenda to Bunia (DR Congo) 220kV Transmission Power Line</h4>
          <p class="project-text">Nkenda to Bunia (DR Congo) 220kV Transmission Power Line. 2012-13: Local Consultant in partnership with GENIVAR Inc. Canada for Environmental Social Impact Assessment (ESIA) and Rese ttlement Action Plan (RAP) for the proposed Nkenda to Bunia (DR Congo) 220kV Transmission Power Line starting from Nkenda substation ). </p>
          
          <p class="clients"> Client: Uganda Electricity Transmission Company/ NELSAP</p>
          
        </div>
      </div>

      <hr>

      <h3 class="col-xs-12 project-sub-heading"><span>C.</span> Solar Power Project</h3>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Proposed 15 MW Solar-PV power Plant and Associated Infrastructure</h4>
          <p class="project-text">Proposed 15 MW Solar-PV power Plant and Associated Infrastructure: March 2014 - October 2014: Environmental and Social Impact Assessment of the proposed 15 MW Solar-PV power Plant and Associated Infrastructure in Mayuge District. Work was being undertaken by JNR Consult which was sub-contracted by JW Technologies Limited. </p>
          
          <p class="clients"> Client: Emerging Power FZC, United Arab Emirates.</p>
          
        </div>
      </div>

      <hr>
      
      <h3 class="col-xs-12 project-sub-heading"><span>D.</span> Water Projects</h3>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>OromGravity Flow Piped Water Supply System</h4>
          <p class="project-text">OromGravity Flow Piped Water Supply System Aug. 2015 –ongoing: Environmental Impact Assessment including catchment situation assessment and preparation of ESMP for Orom Gravity Flow piped water supply system for 10 sub-counties located in Kitgum, Agago and Pader Districts. This was part of the feasibility studies and design for this scheme. JNR was subcontracted by Associated Resource Consultants. </p>
          
          <p class="clients"> Client: Government of Uganda (Ministry of Water and Environment).</p>
          
        </div>
      </div>

      <hr>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Gravity flow Scheme in Bududa District</h4>
          <p class="project-text">Gravity flow Scheme in Bududa District: September 2013 – March 2014: Environmental Impact Assessment for the proposed Bududa –Nabweya. </p>
          
          <p class="clients"> Client: Government of Uganda (Ministry of Water and Environment).</p>
          
        </div>
      </div>
      
      <hr>

      <h2 class="col-xs-12">Ressetlement Action Plans</h2>
      <h3 class="col-xs-12 project-sub-heading"><span>A.</span> Electricity Power Lines</h3>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Lira- Gulu-Nebbi-Arua 132 kV Electricity Transmission Project September 2015 – December 2015</h4>
          <p class="project-text">Resettlement Action Plan for the proposed 132 kV Lira- Gulu-Nebbi-Arua Transmission Line project,315KM (starting from Lira substation). JNR Consult Ltd is sub- consultant to WSP (Canada) </p>
          
          <p class="clients"> Client: Uganda Electricity Transmission Company Limited. Funder: World Bank.</p>
          
        </div>
      </div>

      <hr>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Nkenda to Bunia (DR Congo) 220kV Transmission Power Line 2012-13</h4>
          <p class="project-text">Preliminary Resettlement Action Plan (RAP) for the proposed Nkenda(in Kasese) to Bunia (DR Congo) 220kV Transmission Power Line (starting from Nkenda substation). </p>
          
          <p class="clients"> Client: Nile Equatorial Lakes Subsidiary Action Plan (NELSAP) / Uganda Electricity Transmission Company.</p>
          
        </div>
      </div>

      <hr>

      <h3 class="col-xs-12 project-sub-heading"><span>B.</span> Hydro Power Projects</h3>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Muvumbe: Jan 2015-June 2015</h4>
          <p class="project-text">Updating of Resettlement Action Plan (RAP), Environment and Social Management Plan (ESMP) and Preparation of Environment and Social Action Plan (ESAP) for Muvumbe Hydropower Project (7 MW) in Maziba Sub-county Kabale District. </p>
          
          <p class="clients"> Client: Muvumbe Hydro U Ltd.</p>
          
        </div>
      </div>

    </div>

    <hr>

    <h2 class="col-xs-12">Environment and Social Audit and Compliance projects</h2>
    <h3 class="col-xs-12 project-sub-heading"><span>A.</span> Electricity Power Lines</h3>
    
    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Environmental and Social Compliance Monitoring for Kenya –Tanzania Power Interconnection Project Oct 2016-Oct 2018</h4>
        <p class="project-text">JNR Consult Ltd has been sub-contracted by GOPA – International Energy Consultants (INTEC) GmbH, Germany to carry out Environment and Social compliance monitoring during construction of 400kV overhead line from Isinya in Kenya to Namanga at Kenya – Tanzania boarder. </p>
        
        <p class="clients">Client: Kenya Transmission Company (KETRACO).</p>
        
      </div>
    </div>

    <hr>

    <h3 class="col-xs-12 project-sub-heading"><span>B.</span> Environmental Audit For Rural Elecrification Projects</h3>
    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Kilembe Investment Limited Kasese and Lubirizi Districts September 2015 - December 2015</h4>
        <p class="project-text">Environmental Audit for Rural Elecrification Project for Kilembe Investment Limited Kasese and Lubirizi Districts.</p>
        
        <p class="clients">Client: Kilembe Investment Ltd.</p>
        
      </div>
    </div>

    <hr>

    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Kilembe Investment Limited Kasese District May - August 2013:</h4>
        <p class="project-text">Environmental Audit for Rural Elecrification Project for Kilembe Investment Limited Kasese District.</p>
        
        <p class="clients">Client: Kilembe Investment Ltd.</p>
        
      </div>
    </div>

    <hr>

    <h3 class="col-xs-12 project-sub-heading"><span>C.</span> Hydro Power Projects</h3>
    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Environment and Social Audit for Muvumbe Hydropower October - December 2016</h4>
        <p class="project-text">JNR-Consult was contracted by Muvumbe Hydro Uganda Ltd. to carry out Annual Environment and Social Audit for the project which is still under construction. Issues to audit include compliance to ESMP & ESAP, approval conditions for NEMA and other permits as well as IFC Performance Standards.</p>
        
        <p class="clients">Client: Muvumbe Hydro Uganda Ltd. Funder: KfW/GETFIT.</p>
        
      </div>
    </div>

    <hr>

    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Environmental and Social Compliance Monitoring for Muvumbe Hydro power project. April 2016-June 2017</h4>
        <p class="project-text">JNR-Consult was contracted by Muvumbe Hydro Uganda Ltd. to carry out quarterly monitoring of Environmental and Social Compliance including occupational Health and Saftey during construction. It also involves preparation of reports to be submitted by the client to funders and regulatory authorities in Uganda.</p>
        
        <p class="clients">Client: Muvumbe Hydro Uganda Ltd. Funder: KfW/GETFIT.</p>
        
      </div>
    </div>
    
    <hr>
    
    <h3 class="col-xs-12 project-sub-heading"><span>D.</span> Telecommunication Masts</h3>
    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Environment and Social Audit for 3 Telecommunication Towers in Kampala January –May 2014</h4>
        <p class="project-text">JNR-Consult Ltd was contracted by EATON TOWERS Uganda / GOCHE Ltd to carry out a Compliance Assessment and Audit of 3 Telecommunication Towers in Kampala (Kawempe, Rubaga, Wandegeya) Kampala.</p>
        
        <p class="clients">Client: EATON TOWERS Uganda / GOCHE Ltd.</p>
        
      </div>
    </div>

      <hr>

      <h2 class="col-xs-12">Training</h2>

      <div class="row">
        <div class="col-md-6">
            <img class="project-images img-fluid rounded mb-3 mb-md-0" src="/images/projects/Muvumbe_Health_and_safety.jpg" style="width: 100%;" alt="" />
        </div>
        <div class="col-md-5">
          <h4 class='project-name'>Occupational Health and Safety Training for Muvumbe Hydropower 20th December 2016</h4>
          <p class="project-text">JNR-Consult Ltd was contracted by Muvumbe Hydro Uganda Ltd to carry out an Occupational Health and Safety Training as part of Health and Safety Audit.</p>
          
        </div>
      </div>

      <hr>

      <div class="row">

        <div class="col-md-12" style="padding-left: 40px;">
          <h4 class='project-name'>Training in the Eight National Consultants’ Forum for UACE 16th Aug 2016</h4>
          <p class="project-text">Training of Uganda Association of Consulting Engineers in Environment and Social Impact Assessment, ESMP preparation and monitoring as well as Resettlement Action Plan.</p>
          
          <p class="clients">Client: Uganda Association of Consulting Engineers (UACE).</p>
          
        </div>
      </div>

    <hr>

    <div class="row">

      <div class="col-md-12" style="padding-left: 40px;">
        <h4 class='project-name'>Briquette Project</h4>
        <p class="project-text">Supplying of Final Briquetting machines and Training of users by JNR Consult.</p>
      </div>
      
    </div>

      <hr>
    <!-- /.container -->
    </div>

@endsection

@section('scripts')
      
  <script>

    $(document).ready(function(){
      
      if(window.innerWidth() > 991){
        let images = $(".project-images"); //.css("height", );
        for(i=0; i < images.length; i++){
          expHeight = $(images[i]).parents('.col-md-6').siblings(".col-md-5").height();
          if(expHeight > $(images[i]).height())
            $(images[i]).css("height", expHeight);
        };
      }

    });
      
  </script>

@endsection
