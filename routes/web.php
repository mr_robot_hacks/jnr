<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\JNRInquiry;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/contact-us', function () {
    return view('contact');
});

Route::post('/contact-us', function (Request $request) {

	// $to = "admin@jnr-consult.org";
	$to = "vincenthokie@gmail.com";
	$subject = "JNR Inquiry";
	$txt = "You have an email recieved from the website, the name of the user is " . $request->name . " with email ".$request->email.". This is what they had to say..". $request->comments ."";
	$headers = "From: ". $request->email . "\r\n";
	
	Mail::to($to)
	->send(new JNRInquiry($txt));

    return view('contact')->with("message", "An email has been sent to us. We will get back to you as soon as possible.");
});

Route::get('/projects', function () {
    return view('projects');
});

Route::get('/services', function () {
	return view('services');
});